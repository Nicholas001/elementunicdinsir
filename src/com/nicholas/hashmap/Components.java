package com.nicholas.hashmap;

import java.util.HashMap;
import java.util.Map;

public class Components {

    public static Map addingElementsToHashMap(int[] sirDeNumereBrut) {
        Map<Integer, Integer> sirDeNumere = new HashMap<>();

        for (int x = 0; x < sirDeNumereBrut.length; x++) {
            if (sirDeNumere.containsKey(sirDeNumereBrut[x])) {
                int z = sirDeNumere.get(sirDeNumereBrut[x]);
                sirDeNumere.put(sirDeNumereBrut[x], z + 1);
            } else {
                sirDeNumere.put(sirDeNumereBrut[x], 1);
            }
        }
        System.out.println("Din Array-ul dat s-a format urmatorul HashMap:");
        System.out.println(sirDeNumere.toString());
        return sirDeNumere;
    }
}
