package com.nicholas.opperations;

import com.nicholas.hashmap.Components;

import java.util.Map;

public class Opperations {

    public void preformOperations(int[] sirDeNumereBrut) {

        Map<Integer, Integer> sirDeNumere ;
        sirDeNumere = Components.addingElementsToHashMap(sirDeNumereBrut);

        for (int x = 0; x < sirDeNumereBrut.length; x++) {
            if (sirDeNumere.get(sirDeNumereBrut[x]) == 1) {
                System.out.println("Elementul " + sirDeNumereBrut[x] + " este primul numar unic din sirul dat. ");
                break;
            }
        }

        for (int x = sirDeNumereBrut.length - 1; x >= 0; x--) {
            if (sirDeNumere.get(sirDeNumereBrut[x]) == 1) {
                System.out.println("Elementul " + sirDeNumereBrut[x] + " este ultimul numar unic din sirul dat. ");
                break;
            }
        }
    }
}
